export default interface User {
  id?: number;
  login: string;
  name: string;
  password: string;
  createdData?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
